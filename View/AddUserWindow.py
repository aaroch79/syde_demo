import sys
from PyQt5.QtWidgets import QMainWindow, QWidget, QLabel, QApplication, QDesktopWidget, QPushButton, QLineEdit
from PyQt5.QtCore import QThreadPool, QDate, QTime, QDateTime, Qt
from PyQt5.QtGui import QPainter, QPainterPath
from PyQt5 import QtQuick, QtGui, QtTest

from Controller.Threads import WorkerAddUser
from Controller.Camera import Camera
from View import DetectionWindow


class AddUserWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.title = 'Add User'
        self.font = QtGui.QFont("Helvetica Neue")

        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        width = QDesktopWidget().screenGeometry().width()
        height = QDesktopWidget().screenGeometry().height()
        self.setGeometry(0, 0, width, height)
        self.font = QtGui.QFont("Helvetica Neue")
        self.keyPressEvent = self.detectKeyPress

        count_lbl = QLabel(self)
        name_input = QLineEdit(self)
        add_user_btn = QPushButton(self)
        camera_view = QLabel(self)

        self.font.setPointSize(30)

        count_lbl.resize(width, 60)
        count_lbl.move(0, 60)
        count_lbl.setFont(self.font)
        count_lbl.setStyleSheet("background-color:lightgray;")
        count_lbl.setAlignment(Qt.AlignCenter)

        name_input.resize(400, 60)
        name_input.move(width/2-400, height/2-250)
        name_input.setStyleSheet(
            "background-color:gray; border-width: 2px; border-radius: 10px;")
        name_input.setPlaceholderText("Full Name")
        name_input.setAlignment(Qt.AlignCenter)
        name_input.setFocusPolicy(Qt.ClickFocus)
        name_input.setFont(self.font)

        add_user_btn.resize(200, 60)
        add_user_btn.setText("Add User")
        add_user_btn.move(width/2+120, height/2-250)
        add_user_btn.setStyleSheet(
            "background-color:darkred; border-width: 2px; border-radius: 10px;")
        add_user_btn.setFont(self.font)
        add_user_btn.clicked.connect(self.add_user)

        camera_view.resize(800, 600)
        camera_view.move(width/2-400, height/2-150)
        camera_view.setStyleSheet("background-color: blue;")

        self.name_input = name_input
        self.add_user_btn = add_user_btn
        self.camera_view = camera_view
        self.count_lbl = count_lbl

        self.show()

    def add_user(self):
        if self.name_input.text().strip() == "":
            for i in range(1, 3):
                self.setStyleSheet("background-color:red;")
                self.name_input.setStyleSheet("background-color:white")
                QtTest.QTest.qWait(1000)
                self.setStyleSheet("background-color:white;")
                self.name_input.setStyleSheet("background-color:red")
            self.name_input.setStyleSheet("background-color:gray")
            return
        name = self.name_input.text().strip().replace(" ", "_")
        self.worker = WorkerAddUser(name=name)
        self.threadpool = QThreadPool()
        self.worker.signals.change_image.connect(self.updateImageFrame)
        self.worker.signals.image_count.connect(self.updateCount)
        self.threadpool.start(self.worker)

    def updateCount(self, i):
        self.count_lbl.setText(i)

    def updateImageFrame(self, image):
        self.camera_view.setPixmap(QtGui.QPixmap.fromImage(image))

    def detectKeyPress(self, key):
        if key.key() == Qt.Key_Q:
            self.cams = DetectionWindow.DetectionWindow(False)
            self.cams.show()
            self.close()

    def startDetectionWindow(self, isFinished):
        if isFinished:
            if self.name_input.text().strip() == "":
                self.cams = DetectionWindow.DetectionWindow(newImages=False)
            else:
                self.cams = DetectionWindow.DetectionWindow(newImages=True)
            self.cams.show()
            self.close()


if __name__ == '__main__':
    app = QApplication([])
    ex = AddUserWindow()
    sys.exit(app.exec_())
