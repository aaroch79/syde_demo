import sys
from PyQt5.QtWidgets import QMainWindow, QWidget, QLabel, QApplication, QDesktopWidget, QPushButton, QLineEdit
from PyQt5.QtCore import QThreadPool, QDate, QTime, QDateTime, Qt, QThread
from PyQt5.QtGui import QPainter, QPainterPath
from PyQt5 import QtQuick, QtGui

from Controller.Threads import WorkerTrain, WorkerStartCamera, WorkerStartDetection
from View import AddUserWindow


class DetectionWindow(QWidget):
    def __init__(self, new_images=False):
        super().__init__()
        self.title = 'Demo'
        self.font = QtGui.QFont("Helvetica Neue")

        self.worker_camera = WorkerStartCamera()
        self.threadpool = QThreadPool()
        self.threadpool.setMaxThreadCount(4)

        self.worker_camera.signals.change_image.connect(self.updateImageFrame)
        self.worker_camera.signals.current_time.connect(self.updateTime)
        self.worker_camera.signals.camera_ready.connect(self.start_training)

        self.threadpool.globalInstance().start(self.worker_camera)

        self.keyPressEvent = self.detectKeyPress
        # self.threadpool.start(self.worker)
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        width = QDesktopWidget().screenGeometry().width()
        height = QDesktopWidget().screenGeometry().height()
        self.setGeometry(0, 0, width, height)

        time_lbl = QLabel(self)
        date_lbl = QLabel(self)
        camera_view = QLabel(self)
        information_lbl = QLabel(self)

        camera_view.resize(800, 600)
        camera_view.move(20, height-600-80)
        camera_view.setStyleSheet("background-color: blue;")

        self.font.setPointSize(120)
        self.font.setBold(True)
        time_lbl.resize(800, 100)

        time_lbl.move(width/2+10, height-600-80)
        time_lbl.setAlignment(Qt.AlignCenter)
        time_lbl.setFont(self.font)

        self.font.setPointSize(50)
        date_lbl.resize(800, 100)
        date_lbl.move(width/2+10, height-600-80+100)
        date_lbl.setText("{}".format(
            QDate.currentDate().toString("dddd, MMMM d, yyyy")))
        date_lbl.setFont(self.font)
        date_lbl.setAlignment(Qt.AlignCenter)

        self.font.setPointSize(25)
        self.font.setBold(False)
        information_lbl.setText("To employee user press 'A'")
        information_lbl.resize(width, 25)
        information_lbl.setAlignment(Qt.AlignCenter)
        information_lbl.move(0, height-50)
        information_lbl.setStyleSheet("color: gray;")
        information_lbl.setFont(self.font)

        self.time_lbl = time_lbl
        self.date_lbl = date_lbl
        self.camera_view = camera_view
        self.information_lbl = information_lbl
        # self.container = container
        self.show()

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        qp.setRenderHint(QPainter.Antialiasing)
        self.drawContainer(qp)
        qp.end()

    def drawContainer(self, qp):
        width = QDesktopWidget().screenGeometry().width()
        height = QDesktopWidget().screenGeometry().height()
        path = QPainterPath()
        col = QtGui.QColor(207, 207, 207)
        qp.setBrush(col)
        path.addRect(width/2+20, height-600-80+200, width/2-60, 600-200)
        qp.drawPath(path)

    def detectKeyPress(self, key):
        if key.key() == Qt.Key_A:
            self.cams = AddUserWindow.AddUserWindow()
            self.cams.show()
            self.close()

    def updateTime(self, i):
        self.time_lbl.setText("{}".format(i))

    def updateImageFrame(self, image):
        self.camera_view.setPixmap(QtGui.QPixmap.fromImage(image))

    def start_training(self, is_camera_ready):
        if is_camera_ready:
            self.worker_camera.signals.camera_ready.disconnect()
            self.worker_train = WorkerTrain()
            self.worker_train.signals.is_training.connect(self.set_status)
            self.worker_train.signals.face_detection_obj.connect(
                self.detection_object_aquired)
            self.threadpool.globalInstance().start(self.worker_train)

    def start_employee_found(self, name):
        self.date_lbl.setText(name)

    def set_status(self, is_training):
        print("Status")
        if is_training:
            self.setStyleSheet("background-color:red;")
            self.date_lbl.setText("Retraining with new images please wait...")
        else:
            self.date_lbl.setText("{}".format(
                QDate.currentDate().toString("dddd, MMMM d, yyyy")))
            self.setStyleSheet("background-color:white;")

    def detection_object_aquired(self, obj):
        self.threadpool.clear()
        self.worker_detection = WorkerStartDetection(obj)
        self.worker_detection.signals.change_image.connect(
            self.updateImageFrame)
        self.worker_detection.signals.employee_name.connect(
            self.start_employee_found)
        self.threadpool.start(self.worker_detection)


if __name__ == '__main__':
    app = QApplication([])
    ex = DetectionWindow()
    sys.exit(app.exec_())
