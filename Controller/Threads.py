# worker.py
from PyQt5.QtCore import QThread, QObject, pyqtSignal, pyqtSlot, QRunnable
from PyQt5.QtCore import QDate, QTime, QDateTime, Qt

from PyQt5 import QtGui, QtTest
import cv2
import time
import os
from . import Camera
from . import FaceDetection


class WorkerSignals(QObject):
    current_time = pyqtSignal(str)
    change_image = pyqtSignal(QtGui.QImage)
    image_count = pyqtSignal(str)
    image_capture_finished = pyqtSignal(bool)
    is_training = pyqtSignal(bool)
    employee_name = pyqtSignal(str)
    camera_ready = pyqtSignal(bool)
    face_detection_obj = pyqtSignal(object)


class WorkerStartCamera(QRunnable):
    def __init__(self, name=""):
        super(WorkerStartCamera, self).__init__()
        self.signals = WorkerSignals()
        self.name = name
        self.count = 50
        self.current_image = None

    @pyqtSlot()
    def run(self):
        cam = Camera(0)
        cam.start_camera()
        while True:
            ret, frame = cam.cap.read()
            time = QTime.currentTime().toString(Qt.DefaultLocaleLongDate).replace("EST", "")
            if ret:
                self.signals.camera_ready.emit(True)
                # https://stackoverflow.com/a/55468544/6622587
                rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                self.current_image = frame
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QtGui.QImage(
                    rgbImage.data, w, h, bytesPerLine, QtGui.QImage.Format_RGB888)
                p = convertToQtFormat.scaled(
                    800, 600, Qt.KeepAspectRatioByExpanding)
                self.signals.change_image.emit(p)
                self.signals.current_time.emit(time)


class WorkerStartDetection(QRunnable):
    def __init__(self, detection_obj):
        super(WorkerStartDetection, self).__init__()
        self.signals = WorkerSignals()
        self.detection = detection_obj

    @pyqtSlot()
    def run(self):
        cam = Camera(0)
        cam.start_camera()
        while True:
            ret, frame = cam.cap.read()
            time = QTime.currentTime().toString(Qt.DefaultLocaleLongDate).replace("EST", "")
            if ret:
                # https://stackoverflow.com/a/55468544/6622587
                rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                try:
                    image_response, name = self.detection.classify_image(
                        rgbImage)
                except:
                    continue
                print("Got image")
                rgbImage = cv2.cvtColor(image_response, cv2.COLOR_BGR2RGB)
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QtGui.QImage(
                    rgbImage.data, w, h, bytesPerLine, QtGui.QImage.Format_RGB888)
                p = convertToQtFormat.scaled(
                    800, 600, Qt.KeepAspectRatioByExpanding)
                self.signals.change_image.emit(p)
                self.signals.employee_name.emit(name)


class WorkerTrain(QRunnable):
    def __init__(self, name=""):
        super(WorkerTrain, self).__init__()
        self.detection = None
        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        self.signals.is_training.emit(True)
        self.detection = FaceDetection()
        while False in self.detection.is_ready[:2]:
            print(self.detection.is_ready)
            self.signals.is_training.emit(True)
        self.detection.train()
        while False in self.detection.is_ready:
            self.signals.is_training.emit(True)
        self.signals.is_training.emit(False)
        self.signals.face_detection_obj.emit(self.detection)


class WorkerAddUser(QRunnable):
    def __init__(self, name=""):
        super(WorkerAddUser, self).__init__()
        self.signals = WorkerSignals()

        self.name = name
        self.count = 50

    @pyqtSlot()
    def run(self):
        cam = Camera(0)
        cam.start_camera()
        user_images = []
        output_dir = "./data/datasets/{}".format(self.name)
        for i in range(0, self.count):
            ret, frame = cam.cap.read()
            QtTest.QTest.qWait(100)
            if ret:
                if not os.path.exists(output_dir):
                    os.mkdir(output_dir)
                cv2.imwrite(os.path.join(
                    output_dir, "{}-{}.jpg".format(i, time.time())), frame)

                rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QtGui.QImage(
                    rgbImage.data, w, h, bytesPerLine, QtGui.QImage.Format_RGB888)
                p = convertToQtFormat.scaled(
                    800, 600, Qt.KeepAspectRatioByExpanding)
                count_lbl = "{}/{}".format(i+1, self.count)
                self.signals.change_image.emit(p)
                self.signals.image_count.emit(count_lbl)
        self.signals.image_capture_finished.emit(True)
