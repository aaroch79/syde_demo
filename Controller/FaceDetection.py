# USAGE
# python extract_embeddings.py --dataset dataset --embeddings output/embeddings.pickle \
#	--detector face_detection_model --embedding-model openface_nn4.small2.v1.t7

# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import imutils
import pickle
import cv2
import os
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC


# https://www.pyimagesearch.com/2018/09/24/opencv-face-recognition/


class FaceDetection:
    def __init__(self, dataset_path="data/datasets", output_path="data/output", detection_path="data/detection", confidence=0.5):
        self.is_ready = [False, False, False]
        self.dataset_path = dataset_path
        self.detection_path = detection_path
        self.output_path = output_path
        self.confidence = confidence
        self._load_model_and_embedder()
        self._dataset_initialization()

    def _load_model_and_embedder(self):
        print("[INFO] loading face detector...")
        protoPath = os.path.join(self.detection_path, "deploy.prototxt")
        modelPath = os.path.join(
            self.detection_path, "res10_300x300_ssd_iter_140000.caffemodel")

        # Used to localize faces in an image
        self.detector = cv2.dnn.readNetFromCaffe(protoPath, modelPath)

        # Loading serialzed face embedding model from disk
        print("[INFO] Loading seialized embeddings...")
        # Responsable for extracting facial embeddings
        self.embedder = cv2.dnn.readNetFromTorch(
            os.path.join(self.detection_path, "openface_nn4.small2.v1.t7"))
        self.is_ready[0] = True
        return

    def _dataset_initialization(self):
        imagePaths = list(paths.list_images(self.dataset_path))
        knownEmbeddings = []
        knownNames = []
        total = 0

        for (i, imagePath) in enumerate(imagePaths):
            print("[INFO] Processings image {}/{}".format(i+1, len(imagePaths)))
            name = imagePath.split(os.path.sep)[-2]

            # Loading the iimage, resize it to a width of 600 pixels while
            # maintaining aspect ratio and grab image dimentions
            image = cv2.imread(imagePath)
            image = imutils.resize(image, width=600)
            (h, w) = image.shape[:2]

            # Construct a blob from the image
            imageBlob = cv2.dnn.blobFromImage(cv2.resize(
                image, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0), swapRB=False, crop=False)
            # Opencv face detector to localize faces in the input
            self.detector.setInput(imageBlob)
            # Contains a list of the localized face detections
            detections = self.detector.forward()

            if len(detections) > 0:
                # Assuming that each image has one face
                i = np.argmax(detections[0, 0, :, 2])
                confidence = detections[0, 0, i, 2]

                # Only use detections with our confidence
                if confidence > self.confidence:
                    # Create a bounding box around the face
                    box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                    (startX, startY, endX, endY) = box.astype("int")
                    # Extracting the detected face from image
                    face = image[startY:endY, startX:endX]
                    fH, fW = face.shape[:2]

                    if fW < 20 or fH < 20:
                        # If size is too small skip to next image
                        continue
                    faceBlob = cv2.dnn.blobFromImage(
                        face, 1.0/255, (96, 96), (0, 0, 0), swapRB=True, crop=False)

                    self.embedder.setInput(faceBlob)
                    vec = self.embedder.forward()
                    knownNames.append(name)
                    knownEmbeddings.append(vec.flatten())
                    total += 1

        print("[INFO] Serializing {} encodings".format(total))
        data = {"embeddings": knownEmbeddings, "names": knownNames}
        f = open(os.path.join(self.detection_path, "embeddings.pickle"), "wb")
        f.write(pickle.dumps(data))
        f.close()
        self.is_ready[1] = True
        return

    def train(self):
        print("[INFO] loading embeddings..")
        data = pickle.loads(
            open(os.path.join(self.detection_path, "embeddings.pickle"), "rb").read())
        print("[INFO] encoding labels")
        le = LabelEncoder()
        labels = le.fit_transform(data["names"])
        print("[INFO] training model...")
        # Accept 128-d embeddings of the facce then produce
        # a recognition
        recognizer = SVC(C=1.0, kernel="linear", probability=True)
        recognizer.fit(data["embeddings"], labels)

        # Writing the model and label encoder to the disk
        f = open(os.path.join(self.output_path, "recognizer.pickle"), "wb")
        f.write(pickle.dumps(recognizer))
        f.close()

        f = open(os.path.join(self.output_path, "lb.pickle"), "wb")
        f.write(pickle.dumps(le))
        f.close()
        self.is_ready[2] = True
        return

    def classify_image(self, image):
        # Loading the face recognition model that we trained
        # along withthe label encoder
        recognizer = pickle.loads(
            open(os.path.join(self.output_path, "recognizer.pickle"), "rb").read())
        le = pickle.loads(
            open(os.path.join(self.output_path, "lb.pickle"), "rb").read())

        image = imutils.resize(image, width=600)
        h, w = image.shape[:2]
        imageBlob = cv2.dnn.blobFromImage(cv2.resize(
            image, (300, 300)), 1.0, (300, 300), (104.0, 177.0), swapRB=False, crop=False)
        # Open cv face detector
        self.detector.setInput(imageBlob)
        detections = self.detector.forward()

        for i in range(0, detections.shape[2]):
            # Getting the confidence
            confidence = detections[0, 0, i, 2]

            if confidence > self.confidence:
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                startX, startY, endX, endY = box.astype("int")

                face = image[startY:endY, startX:endX]

                fH, fW = face.shape[:2]

                if fW < 20 or fH < 20:
                    continue
                faceBlob = cv2.dnn.blobFromImage(
                    face, 1.0 / 255, (96, 96), (0, 0, 0), swapRB=True, crop=False)
                self.embedder.setInput(faceBlob)
                # Passing the image to our SVC to get the WHO in out ROI
                vec = self.embedder.forward()

                # Performing a classification on the recognized face

                preds = recognizer.predict_proba(vec)[0]
                # Getting the greatest item with the greatest probability
                j = np.argmax(preds)
                proba = preds[j]
                name = le.classes_[j]
                if proba * 100 > 80:
                    # Drawing out bounding box
                    name_lbl = "{}: {:2f}%".format(name, proba * 100)
                    y = startY - 10 if startY - 10 > 10 else startY + 10
                    cv2.rectangle(image, (startX, startY),
                                  (endX, endY), (0, 0, 255), 2)
                    cv2.putText(image, name_lbl, (startX, y),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
                    return image, name_lbl
                return None
        return False


# if __name__ == "__main__":
#     import sys

#     sys.path.append("..")
#     FaceDetection()
