import cv2
from PyQt5.QtCore import QThread, QObject, pyqtSignal, pyqtSlot, Qt
import os
import time


class Camera:
    def __init__(self, camera_num=0):
        self.cap = None
        self.camera_num = camera_num

    def start_camera(self):
        self.cap = cv2.VideoCapture(self.camera_num)

    def close_camera(self):
        self.cap.release()

    def __str__(self):
        return 'OpenCV Camera {}'.format(self.camera_num)
