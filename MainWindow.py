import sys
from PyQt5.QtWidgets import QMainWindow, QWidget, QApplication
import PyQt5.Qt

from View import DetectionWindow


class MainWindow(QWidget):
    def __init__(self, newImages=False):
        super().__init__()
        self.cams = DetectionWindow.DetectionWindow(newImages)
        self.cams.show()
        self.close()


if __name__ == '__main__':
    app = QApplication([])
    ex = MainWindow()
    sys.exit(app.exec_())
